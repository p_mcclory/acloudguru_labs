#! /usr/bin/env python

def app(environ, start_response):
    print(environ)
    """Simplest possible application object"""
    data = 'it works!\n'
    status = '200 OK'
    response_headers = [
        ('Content-type','text/plain'),
        ('Content-Length', str(len(data)))
    ]
    start_response(status, response_headers)
    #return iter([data])
    #http://stackoverflow.com/questions/22974024/gunicorn-python3-4-and-3-3-sends-in-response-only-headers-without-data
    return [bytes(data, 'utf-8')]
