#! /bin/bash -x

# install dependencies
yum update -y
yum install -y python35 python35-pip git nginx
pip-3.5 install gunicorn
pip install supervisor

git clone https://bitbucket.org/p_mcclory/acloudguru_python_site /app

cp /app/conf/nginx.conf /etc/nginx/nginx.conf
cp /app/conf/supervisord.conf /etc/
cp /app/conf/supervisord.init /etc/init.d/supervisord
chmod a+x /etc/init.d/supervisord

chkconfig nginx on
chkconfig supervisord on

service supervisord start
sleep 2
service nginx start
